package autocomplete.trie;

import autocomplete.Candidate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * An implementation of {@link Candidate} that is used by a {@link Trie}.
 */
public class TrieCandidate implements Candidate {

    private Integer confidence;
    private String word;

    /**
     * Constructs a new {@link TrieCandidate}.
     *
     * @param confidence the confidence
     * @param word       the word
     */
    TrieCandidate(final Integer confidence, final String word) {
        this.confidence = confidence;
        this.word = word;
    }

    /**
     * {@inheritDoc}
     *
     * @return the confidence
     */
    @Override
    public Integer getConfidence() {
        return confidence;
    }

    /**
     * {@inheritDoc}
     *
     * @return the word
     */
    @Override
    public String getWord() {
        return word;
    }

    /**
     * Compares this {@link TrieCandidate} to a different {@link Object}.
     *
     * @param object a different {@link Object}
     * @return a negative integer, zero, or a positive integer as this {@link TrieCandidate}
     * is less than, equal to, or greater than the specified {@link Object}
     */
    @Override
    public int compareTo(final Object object) {
        final Candidate candidate = (Candidate) object;
        if (this.confidence.equals(candidate.getConfidence())) {
            // sort by word
            return word.compareTo(candidate.getWord());
        } else {
            // sort by confidence
            return candidate.getConfidence().compareTo(confidence);
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

}
