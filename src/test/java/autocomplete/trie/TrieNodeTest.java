package autocomplete.trie;

import autocomplete.Candidate;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TrieNodeTest {

    private TrieNode node;

    @Before
    public void setUp() {
        node = new TrieNode();
    }

    @Test
    public void getWord() {
        assertEquals("", node.getWord());
    }

    @Test
    public void getCandidates() {
        node.add("Bulbasaur");
        node.add("Charmander");
        node.add("Squirtle");
        final List<Candidate> list = new ArrayList<>();
        node.getCandidates(list);
        assertEquals(list.size(), 3);
    }

    @Test
    public void getCandidatesWithEmptyNode() {
        final List<Candidate> list = new ArrayList<>();
        node.getCandidates(list);
        assertTrue(list.isEmpty());
    }

}
