package autocomplete;

import autocomplete.trie.TrieAutocompleteProvider;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 * The main {@link SpringBootApplication}.
 */
@SpringBootApplication
public class Application implements CommandLineRunner {

    private TrieAutocompleteProvider trieAutocompleteProvider = new TrieAutocompleteProvider();

    /**
     * Runs this {@link Application}.
     *
     * @param arguments the arguments
     */
    public static void main(final String... arguments) {
        SpringApplication.run(Application.class, arguments);
    }

    /**
     * Runs this {@link Application}.
     *
     * @param arguments the arguments
     */
    @Override
    public void run(final String... arguments) {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            boolean exit = false;
            while (!exit) {
                System.out.println("Enter \"train\" to train the provider");
                System.out.println("Enter \"get\" to get candidates");
                System.out.println("Enter \"exit\" to exit");
                String value = reader.readLine();
                if (value.equalsIgnoreCase("get")) {
                    System.out.println("Waiting for fragment input...");
                    final String fragment = reader.readLine();
                    final List<Candidate> list = trieAutocompleteProvider.getWords(fragment);
                    System.out.println("Candidates:");
                    list.forEach(System.out::println);
                }
                if (value.equalsIgnoreCase("exit")) {
                    System.out.println("Goodbye!");
                    exit = true;
                }
                if (value.equalsIgnoreCase("train")) {
                    System.out.println("Waiting for fragment input...");
                    final String passage = reader.readLine();
                    trieAutocompleteProvider.train(passage);
                    System.out.println("Trained!");
                }
            }
        } catch (final Exception ignored) {
        }
    }

}
