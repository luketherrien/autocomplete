package autocomplete;

import java.util.List;

/**
 * Represents an {@link Object} that can be trained and return
 * {@link Candidate} objects based on input.
 */
public interface AutocompleteProvider {

    /**
     * Returns a {@link List} of {@link Candidate} objects that match the
     * specified fragment.
     *
     * @param fragment the fragment to search for
     * @return a {@link List} of {@link Candidate} objects that match the
     * specified fragment
     */
    List<Candidate> getWords(final String fragment);

    /**
     * Trains this {@link AutocompleteProvider} based on the specified passage.
     *
     * @param passage the passage used to train this {@link AutocompleteProvider}
     */
    void train(final String passage);

}
