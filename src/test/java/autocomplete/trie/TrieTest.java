package autocomplete.trie;

import autocomplete.Candidate;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TrieTest {

    private Trie trie;

    @Before
    public final void setUp() {
        trie = new Trie();
    }

    @Test
    public void getCandidates() {
        trie.add("Bulbasaur");
        final List<Candidate> list = trie.getCandidates("Bulb");
        assertEquals(list.size(), 1);
        final Candidate candidate = list.get(0);
        assertEquals(candidate.getWord(), "Bulbasaur".toLowerCase());
        assertEquals(candidate.getConfidence(), new Integer(1));
    }

    @Test
    public void getCandidatesWithEmptyString() {
        trie.add("Bulbasaur");
        final List<Candidate> list = trie.getCandidates("");
        assertTrue(list.isEmpty());
    }

    @Test
    public void getCandidatesWithDifferentWords() {
        trie.add("Bulbasaur");
        final List<Candidate> list = trie.getCandidates("Char");
        assertTrue(list.isEmpty());
    }

    @Test
    public void getCandidatesWithLongerWord() {
        trie.add("Bulb");
        final List<Candidate> list = trie.getCandidates("Bulbasaur");
        assertTrue(list.isEmpty());
    }

    @Test
    public void getCandidatesWithMultipleMatches() {
        trie.add("Bulb");
        trie.add("Bulbasaur");
        final List<Candidate> list = trie.getCandidates("Bulb");
        assertEquals(list.size(), 2);
        for (final Candidate candidate : list) {
            assertEquals(candidate.getConfidence(), new Integer(1));
        }
    }

}
