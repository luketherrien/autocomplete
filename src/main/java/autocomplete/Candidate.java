package autocomplete;

/**
 * Represents an autocomplete candidate.
 */
public interface Candidate extends Comparable {

    /**
     * Returns the confidence of this {@link Candidate}.
     * The confidence equals the amount of times the candidate was seen in the training input.
     *
     * @return the confidence
     */
    Integer getConfidence();

    /**
     * Returns the word representing this {@link Candidate}.
     *
     * @return the word
     */
    String getWord();

}
