package autocomplete.trie;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TrieCandidateTest {

    private Integer confidence = 0;
    private String word = "";
    private TrieCandidate candidate;

    @Before
    public void setUp() {
        candidate = new TrieCandidate(confidence, word);
    }

    @Test
    public void getConfidence() {
        assertEquals(confidence, candidate.getConfidence());
    }

    @Test
    public void getWord() {
        assertEquals(word, candidate.getWord());
    }

}
