package autocomplete.trie;

import autocomplete.Candidate;

import java.util.*;

/**
 * Represents a trie data structure.
 */
class Trie {

    /**
     * The root node.
     */
    private TrieNode node;

    Trie() {
        node = new TrieNode();
    }

    /**
     * Returns a {@link List} of {@link Candidate} objects that match the
     * specified fragment.
     *
     * @param fragment the fragment to search for
     * @return a {@link List} of {@link Candidate} objects that match the
     * specified fragment
     */
    List<Candidate> getCandidates(
            final String fragment
    ) {
        final List<Candidate> candidates = new ArrayList<>();
        if (fragment != null) {
            // split the fragment into individual characters
            final String[] list = fragment.split("");
            TrieNode node = this.node;
            for (final String item : list) {
                final String key = item.toLowerCase();
                // make sure that the map contains the specified key
                if (node.getMap().containsKey(key)) {
                    // recurse down as far as possible
                    node = node.getMap().get(key);
                    // add the last node as a candidate
                    if (node.getEnd()) {
                        candidates.add(new TrieCandidate(node.getCount(), node.getWord()));
                    }
                } else {
                    return new ArrayList<>();
                }
            }
            // check for leaf node
            if (node != null && node.getParent() != null) {
                // add all candidates
                node.getMap().values().forEach(item -> item.getCandidates(candidates));
            }
        }
        return candidates;
    }

    /**
     * Adds the specified word to the data structure.
     *
     * @param word the word to be added
     */
    void add(final String word) {
        node.add(word);
    }

}
