## Summary

This document describes how to run this application.

## Requirements

This application requires the following tools:

* JDK and JRE `1.8.x`
* Maven `3.x`

## How to Run

```
$ mvn spring-boot:run
```

### Command Line Interface

When this application is run, it starts a command line interface.

The command line interface accepts the following three commands:

* `train` - waits for input to train the autocomplete provider
* `get` - returns autocomplete candidates that match the input
* `exit` - exits the application

## How to Test

```
$ mvn test
```
