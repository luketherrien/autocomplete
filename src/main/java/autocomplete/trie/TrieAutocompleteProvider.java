package autocomplete.trie;

import autocomplete.AutocompleteProvider;
import autocomplete.Candidate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * An {@link AutocompleteProvider} implementation that uses a {@link Trie}.
 */
public class TrieAutocompleteProvider implements AutocompleteProvider {

    private Trie trie;

    public TrieAutocompleteProvider() {
        trie = new Trie();
    }

    /**
     * {@inheritDoc}
     *
     * @param fragment the fragment to search for
     * @return a {@link List} of {@link Candidate} objects that match the
     * specified fragment
     */
    @Override
    public List<Candidate> getWords(final String fragment) {
        final List<Candidate> list = trie.getCandidates(fragment);
        // sort the list
        Collections.sort(list);
        return list;
    }

    /**
     * {@inheritDoc}
     *
     * @param passage the passage used to train this {@link AutocompleteProvider}
     */
    @Override
    public void train(final String passage) {
        if (passage != null) {
            String sanitizedPassage = passage;
            // TODO sanitize passage better
            if (sanitizedPassage.endsWith(".")) {
                sanitizedPassage = sanitizedPassage.substring(0, sanitizedPassage.length() - 1);
            }
            // split the passage into words and add each word to the trie
            Arrays.stream(sanitizedPassage.split(" ")).forEach(item -> trie.add(item));
        }
    }

}
