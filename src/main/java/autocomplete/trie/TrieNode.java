package autocomplete.trie;

import autocomplete.Candidate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a trie node.
 */
class TrieNode {

    private Integer count = 0;
    private Boolean end = false;
    private Map<String, TrieNode> map = new HashMap<>();
    private String key;
    private TrieNode parent;

    /**
     * Represents whether this {@link TrieNode} represents a trained word.
     *
     * @return whether this {@link TrieNode} represents a trained word
     */
    Boolean getEnd() {
        return end;
    }

    /**
     * Returns the number of times the word was trained.
     *
     * @return the number of times the word was trained
     */
    Integer getCount() {
        return count;
    }

    /**
     * Returns a {@link Map} of character / {@link TrieNode} key / value pairs.
     *
     * @return a {@link Map} of character / {@link TrieNode} key / value pairs
     */
    Map<String, TrieNode> getMap() {
        return map;
    }

    /**
     * Returns the word represented by this {@link TrieNode}.
     *
     * @return the word represented by this {@link TrieNode}
     */
    String getWord() {
        final StringBuilder stringBuilder = new StringBuilder();
        String key = this.key;
        TrieNode parent = this.parent;
        while (parent != null) {
            stringBuilder.append(key);
            key = parent.key;
            // recurse up the trie
            parent = parent.parent;
        }
        return stringBuilder.reverse().toString().toLowerCase();
    }

    /**
     * Gets the parent {@link TrieNode}.
     *
     * @return the parent {@link TrieNode}
     */
    TrieNode getParent() {
        return parent;
    }

    /**
     * Adds the specified word to this {@link TrieNode}.
     * @param word the word to be added
     */
    void add(
            final String word
    ) {
        if (word.length() > 0) {
            final String key = word.substring(0, 1).toLowerCase();
            final TrieNode node = map.getOrDefault(key, new TrieNode());
            node.key = key;
            node.parent = this;
            map.put(key, node);
            node.add(word.substring(1));
        } else {
            count++;
            end = true;
        }
    }

    /**
     * Returns a {@link List} of {@link Candidate} objects of this
     * {@link TrieNode}.
     *
     * @param candidates the input {@link List} of {@link Candidate} objects
     */
    void getCandidates(
            final List<Candidate> candidates
    ) {
        if (end) {
            candidates.add(new TrieCandidate(count, getWord()));
        } else {
            // add all candidates
            map.values().forEach(node -> node.getCandidates(candidates));
        }
    }

}
