package autocomplete.trie;

import autocomplete.Candidate;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TrieAutocompleteProviderTest {

    private TrieAutocompleteProvider autocompleteProvider;

    @Before
    public void setUp() {
        autocompleteProvider = new TrieAutocompleteProvider();
    }

    @Test
    public void getWords() {
        final List<Candidate> list = autocompleteProvider.getWords("");
        assertTrue(list.isEmpty());
    }

    @Test
    public void trainAndGetDifferentWord() {
        autocompleteProvider.train("Bulbasaur");
        final List<Candidate> list = autocompleteProvider.getWords("Charmander");
        assertTrue(list.isEmpty());
    }

    @Test
    public void trainAndGetExampleWords() {
        autocompleteProvider.train("The third thing that I need to tell you is that this thing does not think thoroughly");
        List<Candidate> list = autocompleteProvider.getWords("thi");
        assertEquals(list.size(), 4);
        list = autocompleteProvider.getWords("nee");
        assertEquals(list.size(), 1);
        list = autocompleteProvider.getWords("th");
        assertEquals(list.size(), 7);
    }

    @Test
    public void trainAndGetSameWord() {
        autocompleteProvider.train("Bulbasaur Bulbasaur Bulbasaur");
        final List<Candidate> list = autocompleteProvider.getWords("Bulbasaur");
        assertEquals(list.size(), 1);
        final Candidate candidate = list.get(0);
        assertEquals(candidate.getWord(), "Bulbasaur".toLowerCase());
        assertEquals(candidate.getConfidence(), new Integer(3));
    }

    @Test
    public void trainAndGetWords() {
        autocompleteProvider.train("Bulbasaur Charmander Squirtle");
        final List<Candidate> list = autocompleteProvider.getWords("Bulbasaur");
        assertEquals(list.size(), 1);
        final Candidate candidate = list.get(0);
        assertEquals(candidate.getWord(), "Bulbasaur".toLowerCase());
        assertEquals(candidate.getConfidence(), new Integer(1));
    }

    @Test
    public void trainAndGetWordsLowerAndUpperCase() {
        autocompleteProvider.train("Bulbasaur".toLowerCase());
        autocompleteProvider.train("Bulbasaur".toUpperCase());
        final List<Candidate> list = autocompleteProvider.getWords("Bulbasaur");
        assertEquals(list.size(), 1);
        final Candidate candidate = list.get(0);
        assertEquals(candidate.getWord(), "Bulbasaur".toLowerCase());
        assertEquals(candidate.getConfidence(), new Integer(2));
    }

}
